import FormControl from '@material-ui/core/FormControl';
import { InputLabel, Input , Box, Typography, Button}  from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import axios from 'axios';
import { API_URL } from '../../config';
const useStyles = makeStyles((theme) => ({
  formControl: {
      maxWidth: 350
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function SimpleSelect() {
  const classes = useStyles();
  const [selected, setSelected] = React.useState('');
  const [file, setFile] = React.useState(null);
  const [error, setError] = React.useState("");
  const [msg, setMsg] = React.useState("");
  const handleChange = (event) => {
    setError("");
    setMsg("");
    setSelected(event.target.value);
  };
  const handleFileChange = (event) => {
        let file = event.target.files[0];
        setError("");
	setMsg("");
        if(file) {
            let ext = file.name.split('.').pop();
            if(ext === 'bin') {
                setFile(file)
            } else {
                setError("Please upload .bin file");
                setFile(null)
            }
        }
  };
  const submitData = () => {
      if(!file) {
        setError("Please choose a file to upload");
        return;
      }
      if(selected === '') {
        setError("Please choose a target ECU");
        return;
      }
      var formData = new FormData();
      formData.append('ECU_ID', selected);
      formData.append('file', file);
      axios({
        method: "post",
        url: `http://${API_URL}/uploader`,
        data: formData,
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then(function (response) {
      		if(response.status === 200) {
	           setMsg("Uploaded firmware. Please check logs for flash updates.");
		   setSelected("");
	           document.getElementById('file').value = null;
		   setFile(null);
		}
	})
     .catch(function (response) {
        console.log(response);
     });
  }
  return (
    <div>
      {/* <Box>
        <Typography color="textSecondary" variant="subtitle1">
            Instructions
        </Typography>
        <Typography color="textSecondary" variant="body1">
           1)  Add instructions here
        </Typography>
        <Typography color="textSecondary" variant="body1">
           2)  Add instructions here
        </Typography>
        <Typography color="textSecondary" variant="body1">
           3)  Add instructions here
        </Typography>
      </Box> */}
      <Box mt={3}>
      <FormControl variant="outlined" className={classes.formControl} fullWidth>
        <InputLabel id="demo-simple-select-outlined-label">Target ECU</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={selected}
          onChange={handleChange}
          label="Target ECU"
          fullWidth
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={16}>Driver</MenuItem>
          <MenuItem value={32}>Geo</MenuItem>
          <MenuItem value={48}>Motor</MenuItem>
          <MenuItem value={64}>Sensor</MenuItem>
        </Select>
      </FormControl>
      </Box>
      <Box mt={3} display="flex">
        <Button
            variant="contained"
            component="label"
            style={{width: 200}}
            size="large"
        >
            Upload File
            <input id="file" type="file" name="file" aria-describedby="my-helper-text" accept=".bin" onChange={handleFileChange}  hidden/>
        
        </Button>
        {file && 
            <Box display="flex" alignItems="center" ml={2}>
                <Typography >
                     {file.name}
                </Typography>  
            </Box>
             
        }
      </Box>
      <Box mt={3}>
        <Typography color="error">
            {error}
        </Typography>
      </Box>
      <Box mt={3}>
        <Button size="large" variant="contained" color="primary" onClick={submitData} style={{width: 200}}>
                Submit
        </Button>
      </Box>
     <Box mt={3}>
        <Typography style={{color: "green"}}>
            {msg}
        </Typography>
      </Box>

    </div>
  );
}
