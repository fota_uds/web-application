import { Box, Button, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import React from 'react';
import { API_URL } from '../../config';
const useStyles = makeStyles((theme) => ({
  formControl: {
      maxWidth: 350
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function GetCarData() {
  const classes = useStyles();
  const [logs, setLogs] = React.useState([]);
  React.useEffect(() => {
    getLogs();
  }, [])
  const getLogs = () => {
     axios({
        method: "post",
        url: `http://${API_URL}/gatewaylogs`
      })
      .then(function (response) {
        setLogs(response.data.split('\n'));
     })
     .catch(function (response) {
        console.log(response);
     });
  }
  return (
      <>
      <Box display="flex" color="primary" justifyContent="flex-end">
          <Button variant="outlined" onClick={getLogs}>
              Refresh
          </Button>
      </Box>
      <Box maxHeight={600} overflow="auto">
          {logs.map(l =>  <Typography>
            {l}
          </Typography>)}
      </Box>
     </>
  );
}
