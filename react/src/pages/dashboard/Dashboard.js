import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FirmwareUpdate from './FirmwareUpdate';
import GetCarData from './GetCarData';
import Logs from './Logs';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    maxWidth: 600,
    margin: "auto"
  },
}));

export default function Dashboard() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Firmware Update" {...a11yProps(0)} />
          <Tab label="Get Car Data" {...a11yProps(1)} />
          <Tab label="Logs" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <Box
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
           <FirmwareUpdate/>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
           <GetCarData/>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction} style={{backgroundColor: 'black', color: "greenyellow"}}>
           <Logs/>
        </TabPanel>
      </Box>
    </div>
  );
}
