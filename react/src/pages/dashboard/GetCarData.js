import FormControl from '@material-ui/core/FormControl';
import { InputLabel, TextField , Box, Typography, Button}  from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import axios from 'axios';
import { API_URL } from '../../config';
const useStyles = makeStyles((theme) => ({
  formControl: {
      maxWidth: 350
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
const options = {
  16 : "Driver",
  32 : "Geo",
  48: "Motor",
  64: "Sensor"
}

export default function GetCarData() {
  const classes = useStyles();
  const [selected, setSelected] = React.useState('');
  const [dataID, setdataID] = React.useState(0);
  const [data, setData] = React.useState("");
  const [error, setError] = React.useState("");
  const handleChange = (event) => {
    setError("");
    setData("");
    setSelected(event.target.value);
  };
  const handleDataIDChange = (event) => {
      setData("");
      setdataID(event.target.value)
  };
  const submitData = () => {
      if(selected === '') {
        setError("Please choose a target ECU");
        return;
      }
      var formData = new FormData();
      formData.append('ECU_ID', selected);
      formData.append('DID', dataID);
      axios({
        method: "post",
        url: `http://${API_URL}/dataget`,
        data: formData,
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then(function (response) {
        setData(response.data);
     })
     .catch(function (response) {
        console.log(response);
     });
  }
  return (
    <div>
      {/* <Box>
        <Typography color="textSecondary" variant="subtitle1">
            Instructions
        </Typography>
        <Typography color="textSecondary" variant="body1">
           1)  Add instructions here
        </Typography>
        <Typography color="textSecondary" variant="body1">
           2)  Add instructions here
        </Typography>
        <Typography color="textSecondary" variant="body1">
           3)  Add instructions here
        </Typography>
      </Box> */}
      <Box mt={3}>
      <FormControl variant="outlined" className={classes.formControl} fullWidth>
        <InputLabel id="demo-simple-select-outlined-label">Target ECU</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={selected}
          onChange={handleChange}
          label="Target ECU"
          fullWidth
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {Object.keys(options).map(o => <MenuItem value={o}>{options[o]}</MenuItem>)}
        </Select>
      </FormControl>
      </Box>
      <Box mt={3} display="flex">
        <TextField label="Data ID" variant="outlined" type="number" value={dataID} onChange={handleDataIDChange}/>
      </Box>
      <Box mt={3}>
        <Typography color="error">
            {error}
        </Typography>
      </Box>
      <Box mt={3}>
        <Button size="large" variant="contained" color="primary" onClick={submitData} style={{width: 200}}>
                Submit
        </Button>
      </Box>
      {data && 
      <Box mt={3}>
        {options[selected]} Data:  {data}
      </Box>
      }
    </div>
  );
}
