import paho.mqtt.client as mqttClient
import time

def on_connect(client, userdata, flags, rc):

    if rc == 0:

        print("Connected to broker")

        global Connected                #Use global variable
        Connected = True                #Signal connection

    else:

        print("Connection failed")

def on_message(client, userdata, message):
    print("Message received: "  + message.payload.decode("utf-8"))
    with open('/home/ubuntu/web_app/gateway_logs.txt','a+') as f:
         f.write("Message received: "  + str(message.payload.decode("utf-8")) + "\n")

Connected = False   #global variable for the state of the connection

broker_address= "18.144.135.55"  #Broker address
port = 1883                         #Broker port
user = "fota"                    #Connection username
password = "t3sti9"            #Connection password

client = mqttClient.Client("Python")               #create new instance
client.username_pw_set(user, password=password)    #set username and password
client.on_connect= on_connect                      #attach function to callback
client.on_message= on_message                      #attach function to callback
client.connect(broker_address,port,60) #connect
client.subscribe("logs") #subscribe
client.loop_forever() #then keep listening forever

# import paho.mqtt.publish as publish
# import paho.mqtt.subscribe as subscribe
# import hashlib

# f = None

# def dump_logs(ms, obj, m):
#     global f
#     f.write(str(m.payload))
#     f.write('\n')
#     f.flush()
#     print("logs: " + str(m.payload))
    


# def get_log():
#     global f
#     f = open("/home/ubuntu/web_app/gateway_logs.txt", "a")
#     mqtt_auth = { 'username': 'fota', 'password': 't3sti9' }
#     m=subscribe.callback(dump_logs, "logs", hostname="18.144.135.55", port=1883, client_id="asach", auth=mqtt_auth)
#     f.close()

# get_log()
#init()
# publish_data("files/lpc40xx_ecu_stack.bin")
# publish_data("files/lpc40xx_ecu_stack.bin")
