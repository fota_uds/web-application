import paho.mqtt.publish as publish
import paho.mqtt.subscribe as subscribe
import hashlib

def publish_data(fw_file_name, ecu_id):
    mqtt_auth = { 'username': 'fota', 'password': 't3sti9' }
    md5value = hashlib.md5(open(fw_file_name,'rb').read()).hexdigest()
    payload = str(fw_file_name) + "," + str(ecu_id) + "," + str(md5value)
    print("Payload = ", payload)
    # f = open(fw_file_name,"rb")
    # fw_data = f.read()
    # f.close()
    # byteArray = bytearray(fw_data)
    publish.single("fwData", payload, hostname="18.144.135.55", port=1883, client_id="asach", auth=mqtt_auth)

def request_data(data):
    mqtt_auth = { 'username': 'fota', 'password': 't3sti9' }
    #byteArray = bytearray(data)
    publish.single("debug", data, hostname="18.144.135.55", port=1883, client_id="asach", auth=mqtt_auth)
    m=subscribe.simple("debug_rcv", hostname="18.144.135.55", port=1883, client_id="asach", auth=mqtt_auth, msg_count=1)
    print("Data = ", m.payload)
    return m.payload

