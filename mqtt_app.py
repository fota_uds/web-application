import paho.mqtt.client as mqtt

#broker_url = "18.144.135.55"
#broker_port = 1883

def on_publish(client, userdata, mid):
    print("Data published");

def on_connect(client, userdata, flags, rc):
   print("Connected With Result Code ")

def on_message_from_debug(client, userdata, message):
   print("Message Recieved from debug: "+message.payload.decode())

def on_message_from_fw_tx(client, userdata, message):
   print("Message Recieved from fw: "+message.payload.decode())

def on_message(client, userdata, message):
   print("Message Recieved from Others: "+message.payload.decode())

def publish_firmware(fw_file_name):
    broker_url = "18.144.135.55"
    broker_port = 1883
    client = mqtt.Client()
    #client.on_connect = on_connect
    client.on_publish = on_publish
    client.username_pw_set(username="fota",password="t3sti9")
    #To Process Every Other Message
    #client.on_message = on_message
    client.connect(broker_url, broker_port)

    #client.subscribe("fwTx", qos=1)
   # client.subscribe("debugData", qos=1)
    #client.subscribe("otherData", qos=1)
    #client.message_callback_add("debugData", on_message_from_debug)
    #client.message_callback_add("fwTx", on_message_from_fw_tx)
    print(fw_file_name)
    f = open(fw_file_name,"rb")
    fw_data = f.read()
    f.close()
    byteArray = bytearray(fw_data)
    ret = client.publish("new", payload=byteArray, client_id="web_app")#, qos=0, retain=False)#, hostname=broker_url, port=broker_port, client_id="", keepalive=60, will=None, auth=None, tls=None, protocol=mqtt.MQTTv311, transport="tcp")
    print(ret)
    #client.loop_forever()

publish_firmware('files/lpc40xx_ecu_stack.bin');
