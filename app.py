import os
from flask import Flask, render_template, request, redirect, url_for, abort, send_from_directory
from werkzeug.utils import secure_filename
from mqtt2 import *
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.bin']
app.config['UPLOAD_PATH'] = '/home/ubuntu/web_app/files'

@app.route('/upload')
def upload():
    return render_template('upload.html')

@app.route('/data')
def read_data():
    return render_template('data.html')
    
@app.route('/dataget', methods=['POST'])
@cross_origin()
def get_data():
    ecu_id = request.form['ECU_ID']
    did = request.form['DID']
    payload = str(ecu_id) + "," + str(did)
    return request_data(payload)


@app.route('/uploader', methods=['POST'])
@cross_origin()
def upload_files():
    ecu_id = request.form['ECU_ID']
    uploaded_file = request.files['file']
    filename = str(ecu_id) + '_' + secure_filename(uploaded_file.filename)
    if filename != '':
        file_ext = os.path.splitext(filename)[1]
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            abort(400)
        uploaded_file.save(os.path.join(app.config['UPLOAD_PATH'], filename))
        publish_data('/home/ubuntu/web_app/files/' + filename, str(ecu_id))
    return "Upload success"

@app.route("/submitted", methods=['POST', 'GET'])
def submitted():
    if request.method == 'POST':
        result = request.form

        query = result['query']

        if 'file' not in request.files:
            datapath = 'static/iris.csv'
            plot_id = str(uuid.uuid1())
        else:
            f = request.files['file']
            f.save(secure_filename(f.filename))
            filename = secure_filename(f.filename)
            f.save(os.path.join(app.config['UPLOAD_PATH'],\
                     filename))

        return render_template("submitted.html")

@app.route('/gatewaylogs', methods=['POST'])
@cross_origin()
def getGatewayLogs():
    return send_from_directory("/home/ubuntu/web_app", "gateway_logs.txt", as_attachment=True)

@app.route('/')
@cross_origin()
def hello():
    return "Hello World!"

if __name__ == '__main__':
    app.run(host= '0.0.0.0')
